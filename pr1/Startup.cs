﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(pr1.Startup))]
namespace pr1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
